﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using Outlook = Microsoft.Office.Interop.Outlook;
using Office = Microsoft.Office.Core;
using System.Windows.Forms;

namespace OutlookAddIn2010
{
    public partial class ThisAddIn
    {
        private void ThisAddIn_Startup(object sender, System.EventArgs e)
        {
            AccessContacts("orange.mu");
        }

        private void AccessContacts(string findLastName)
        {
            Outlook.MAPIFolder folderContacts = this.Application.ActiveExplorer().Session.
                GetDefaultFolder(Outlook.OlDefaultFolders.olFolderContacts);
            Outlook.Items searchFolder = folderContacts.Items;
            int counter = 0;
            foreach (Outlook.ContactItem foundContact in searchFolder)
            {
                try
                {
                    if (foundContact.Email1Address != null && foundContact.Email1Address.EndsWith(findLastName))
                    {
                        foundContact.Email1Address = foundContact.Email1Address.Replace("@orange.mu", "@intnet.mu");
                        foundContact.Save();
                        counter++;
                    }
                    if (foundContact.Email2Address != null && foundContact.Email2Address.EndsWith(findLastName))
                    {
                        foundContact.Email2Address = foundContact.Email2Address.Replace("@orange.mu", "@intnet.mu");
                        foundContact.Save();
                        counter++;
                    }
                    if (foundContact.Email3Address != null && foundContact.Email3Address.EndsWith(findLastName))
                    {
                        foundContact.Email3Address = foundContact.Email3Address.Replace("@orange.mu", "@intnet.mu");
                        foundContact.Save();
                        counter++;
                    }
                }
                catch (Exception e)
                {
                    MessageBox.Show(e.Message);
                }
                
            }
            if (counter > 0)
                MessageBox.Show(counter + " email addresses with orange.mu has been migrated to intnet.mu");
        }

        private void ThisAddIn_Shutdown(object sender, System.EventArgs e)
        {
            // Note: Outlook no longer raises this event. If you have code that 
            //    must run when Outlook shuts down, see https://go.microsoft.com/fwlink/?LinkId=506785
        }

        #region VSTO generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InternalStartup()
        {
            this.Startup += new System.EventHandler(ThisAddIn_Startup);
            this.Shutdown += new System.EventHandler(ThisAddIn_Shutdown);
        }
        
        #endregion
    }
}
